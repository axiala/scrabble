package com.arolla;

import com.arolla.scrabble.ScoreRules;
import com.arolla.scrabble.Scrabble;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class ScrabbleShould {

    private ScoreRules rules;

    @Before
    public void initialize() {
        rules = new ScoreRules();
    }

    @Test
    public void return_the_score_eight_for_the_word_hello() {
        //GIVEN
        List<String> dictionary = Collections.emptyList();
        Scrabble game = new Scrabble(dictionary, rules.getScoresTable() );

        //THEN
        assertThat(game.getScore("word")).isEqualTo(8);
    }

    @Test
    public void return_whizzing_as_the_best_word() {
        //GIVEN
        List<String> dictionary = asList("aaaaa", "zzzzzzz");
        List<String> playedWords = asList("aaaaa", "zzzzzzz", "Hello", "whizzing");
        Scrabble game = new Scrabble(dictionary, rules.getScoresTable());

        //WHEN
        game.play(playedWords);

        //THEN
        assertThat(game.getBestPlayedWord()).isEqualTo("whizzing");
    }

    @Test
    public void return_the_histogram_of_the_number_of_played_words_having_same_score() {
        //GIVEN
        List<String> dictionary = asList("aaaaa", "zzzzzzz");
        List<String> playedWords = asList("aaaaa", "zzzzzzz", "Hello", "whizzing", "Hellod", "Hellog");
        Scrabble game = new Scrabble(dictionary, rules.getScoresTable());

        //WHEN
        game.play(playedWords);

        //THEN
        assertThat(game.getHistogram().size()).isEqualTo(3);
    }

    @Test
    public void return_the_words_of_the_best_three_scores() {
        //GIVEN
        List<String> dictionary = asList("aaaaa", "zzzzzzz");
        List<String> playedWords = asList("aaaaa", "zzzzzzz", "Hello", "whizzing", "Hellod", "Hellog", "bravest");
        Scrabble game = new Scrabble(dictionary, rules.getScoresTable());
        int whizzingScore = game.getScore("whizzing");

        //WHEN
        game.play(playedWords);

        //THEN
        assertThat(game.getTopScoresWords(3).get(whizzingScore).size()).isEqualTo(1);
    }

}
