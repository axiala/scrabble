package com.arolla;

import com.arolla.scrabble.WordsReader;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class WordsReaderShould {


    @Test
    public void read_the_list_words_present_in_a_file() {
        //GIVEN
        WordsReader reader = new WordsReader();

        //WHEN
        List<String> words = reader.readWordsFromFile("words.txt");

        //THEN
        assertThat(words.size()).isEqualTo(6);
        assertThat(words.contains("bravery")).isTrue();
    }

}

