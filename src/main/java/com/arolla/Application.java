package com.arolla;

import com.arolla.scrabble.ScoreRules;
import com.arolla.scrabble.Scrabble;
import com.arolla.scrabble.WordsReader;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Application {

    public static void main(String[] args) {
        ScoreRules rules = new ScoreRules();
        WordsReader reader = new WordsReader();
        List<String> dictionary = reader.readWordsFromFile("dictionary.txt");
        List<String> playedWords = reader.readWordsFromFile("playedWords.txt");

        Scrabble game = new Scrabble(dictionary, rules.getScoresTable());

        game.play(playedWords);

        printHelloScore(game);
        printTheBestPlayedWord(game);
        printTheHistogram(game);
        printTheThreeBestScoreAndItsWords(game);
    }

    private static void printTheThreeBestScoreAndItsWords(Scrabble game) {
        Map<Integer, Set<String>> scores = game.getTopScoresWords(3);
        scores.forEach((score, words) -> System.out.println(score + " : " + words.toString() ));
    }

    private static void printTheHistogram(Scrabble game) {
        Map<Integer, Integer> histogram = game.getHistogram();
        histogram.forEach((score, wordsNumber) ->
                System.out.println("For Score " + score + " we have " + wordsNumber + " word"));
        System.out.println("Histogram size is " + histogram.size());
    }

    private static void printTheBestPlayedWord(Scrabble game) {
        String bestPlayedWord = game.getBestPlayedWord();
        System.out.println("Best played word  is: " + bestPlayedWord + "and has score: " + game.getScore(bestPlayedWord));
    }

    private static void printHelloScore(Scrabble game) {
        System.out.println("Score for the word Hello is: " + game.getScore("Hello"));
    }


}
