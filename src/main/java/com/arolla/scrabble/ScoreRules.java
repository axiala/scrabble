package com.arolla.scrabble;

import java.util.HashMap;
import java.util.Map;

public class ScoreRules {

    private HashMap<Integer, Integer> letterScore = new HashMap<>();

    public ScoreRules() {
        buildScoresTable();
    }

    private void buildScoresTable() {
        mapLettersToScore("EAIONRTLSU", 1);
        mapLettersToScore("DG", 2);
        mapLettersToScore("BCMP", 3);
        mapLettersToScore("FHVWY", 4);
        mapLettersToScore("K", 5);
        mapLettersToScore("JX", 8);
        mapLettersToScore("QZ", 10);
    }

    private void mapLettersToScore(String letters, int score) {
        letters.chars()
                .forEach(letter -> this.letterScore.put(letter, score));
    }

    public Map<Integer, Integer> getScoresTable() {
        return this.letterScore;
    }
}
