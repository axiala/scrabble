package com.arolla.scrabble;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WordsReader {


    public List<String> readWordsFromFile(String fileName) {
        List<String> words = new ArrayList<>();
        try {
            URL resource = getClass().getClassLoader()
                    .getResource(fileName);
            if (resource != null) {
                Path path = Paths.get(resource.toURI());
                words = Files.lines(path).collect(Collectors.toList());
            } else {
                throw new FileNotFoundException("The file name: " + fileName + " doesn't exists");
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return words;
    }
}
