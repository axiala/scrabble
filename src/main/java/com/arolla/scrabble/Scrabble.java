package com.arolla.scrabble;

import java.util.*;
import java.util.stream.Collectors;

public class Scrabble {
    private List<String> dictionary;
    private Map<Integer, Integer> rulesTable;
    private int highestScore;
    private String bestPlayedWord;
    private Map<Integer, Set<String>> scores = new HashMap<>();

    public Scrabble(List<String> dictionary, Map<Integer, Integer> rulesTable) {
        this.dictionary = dictionary;
        this.rulesTable = rulesTable;
    }

    public int getScore(String word) {
        return word.toUpperCase()
                .chars()
                .map(character -> rulesTable.getOrDefault(character, 0))
                .reduce(0, (current, previous) -> current + previous);
    }

    public void play(List<String> words) {
        words
            .stream()
            .filter(word -> !this.dictionary.contains(word))
            .forEach(word -> updateGame(word, getScore(word)));
    }


    private void updateGame(String word, int score) {
        updateBestPlayedWord(word, score);
        updateScores(word, score);
    }

    private void updateScores(String word, int score) {
        if(!this.scores.containsKey(score)) {
            this.scores.put(score, new HashSet<>());
        }
        this.scores.get(score).add(word);
    }

    private void updateBestPlayedWord(String word, int score) {
        if(score > highestScore) {
            this.bestPlayedWord = word;
            this.highestScore = score;
        }
    }

    public String getBestPlayedWord() {
        return this.bestPlayedWord;
    }

    public Map<Integer, Integer> getHistogram() {
        return this.scores.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().size(),
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    public Map<Integer, Set<String>> getTopScoresWords(int topLimitNumber) {
        return this.scores.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
                .limit(topLimitNumber)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

    }
}
