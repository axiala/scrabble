# Scrabble Score Kata

This is a java 8 implementation for the scrabble score kata.

## Details

Both dictionary and the played words are fetched from the files dictionay.txt and playedwords.txt found under the project resources folder.

You may run the application through the main method found in the Application class

The application will print the following information in your console:

* The score of the word "hello"
* The best played word
* The histogram presenting the list the words having the same score
* The list of words having the top score 